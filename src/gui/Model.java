package gui;

import base.Clase;
import base.Raza;
import base.Usuario;
import util.Constantes;
import util.Util;

import java.io.*;
import java.sql.*;
import java.util.*;

import static util.Constantes.DEF_RUTA;

/**
 * Created by Fernando on 08/11/2016.
 */
public class Model {
    private String RUTA_BASE;
    private String RUTA_PROPS;
    private Properties config;
    private Connection conexion;

    public Model() {
        RUTA_BASE=Constantes.DEF_RUTA;
        RUTA_PROPS=RUTA_BASE+File.separator+"Config.props";
    }

    /**
     * Registra una raza en la base de datos
     * @param raza Clase a introducir en la Base de datos
     * @throws SQLException posible excepcion por codigoSQL
     */
    public void registrar(Raza raza) throws SQLException {
        String sentenciaSQL = "INSERT INTO raza (raza, skill_1, skill_2, skill_3, skill_ex) VALUES" +
                "(?, ?, ?, ?, ?)";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(1, raza.getRaza());
        sentencia.setString(2, raza.getSkill_1());
        sentencia.setString(3, raza.getSkill_2());
        sentencia.setString(4, raza.getSkill_3());
        sentencia.setString(5, String.valueOf(raza.getSkill_ex()));
        sentencia.execute();
        if (sentencia != null){
            sentencia.close();
        }
    }

    /**
     * Registra una clase en la base de datos
     * @param clase Clase a introducir en la Base de datos
     * @throws SQLException posible excepcion por codigoSQL
     */
    public void registrar(Clase clase) throws SQLException {
        String sentenciaSQL = "INSERT INTO clase (clase, salud, armadura, magia, magias) VALUES" +
                "(?, ?, ?, ?, ?)";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(1, clase.getNombre());
        sentencia.setFloat(2, clase.getSalud());
        sentencia.setString(3, String.valueOf(clase.getArmadura()));
        sentencia.setBoolean(4, clase.isMagia());
        sentencia.setString(5, String.valueOf(clase.getTipoMagia()));
        sentencia.execute();
        if (sentencia != null){
            sentencia.close();
        }
    }

    /**
     * Registra un usuario en la Base de datos
     * @param usuario usuario a registrar
     * @throws SQLException posible excepcion por codigoSQL
     */
    public void registrar(Usuario usuario) throws SQLException {
        String sentenciaSQL = "INSERT INTO usuario(nombre, apellido, fecha, raza, clase) VALUES" +
                "(?, ?, ?, ?, ?)";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(1, usuario.getNombre());
        sentencia.setString(2, usuario.getApellidos());
        java.sql.Date fecha = new java.sql.Date(usuario.getFecha().getTime());
        sentencia.setDate(3, fecha);
        try{
            sentencia.setString(4, usuario.getRaza().getRaza());
        }catch (Exception e){
            sentencia.setString(4, "");
        }
        try{
            sentencia.setString(5, usuario.getClase().getNombre());
        }catch (Exception e){
            sentencia.setString(5, "");
        }
        sentencia.execute();
        if (sentencia != null){
            sentencia.close();
        }
    }

    /**
     * Elimina una raza de la Base de Datos
     * @param clave Nombree de la primary key
     * @throws SQLException posible error en el codigo SQL
     */
    public void eliminarRaza(String clave) throws SQLException {
        modificarUsuario(clave, "raza", "");
        String sentenciaSQL = "DELETE FROM raza WHERE raza.raza = ?";
        PreparedStatement sentencia =conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(1, clave);
        sentencia.executeUpdate();
        if (sentencia!=null){
            sentencia.close();
        }
    }

    /**
     *  Elimina una clase de la base de datos
     * @param clave Primary key de la base de datos
     * @throws SQLException Posible error de la sentencia sql
     */
    public void eliminarClase(String clave) throws SQLException {
        modificarUsuario(clave, "clase", "");
        String sentenciaSQL = "DELETE FROM clase WHERE clase.clase = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(1, clave);
        sentencia.executeUpdate();
        if (sentencia!=null){
            sentencia.close();
        }
    }

    /**
     * Elimina a un usuario de la base de datos
     * @param clave Primary Key de la BBDD
     * @throws SQLException Posibble error de sentencia SQL
     */
    public void eliminarUsuario(String clave) throws SQLException {
        String sentenciaSQL = "DELETE FROM usuario WHERE nombre = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(1, clave);
        sentencia.executeUpdate();
        if (sentencia != null){
            sentencia.close();
        }
    }

    /**
     * Elimina una raza y pone otra en su lugar
     * @param clave id de la raza
     * @param raza clase Raza
     */
    public void modificar(String clave, Raza raza) throws SQLException {
        String SentenciaSQL = "UPDATE raza SET " +
                "raza = ?, " +
                "skill_1 = ?, " +
                "skill_2 = ?, " +
                "skill_3 = ?, " +
                "skill_ex = ? " +
                "WHERE raza = ?";
        PreparedStatement sentencia = conexion.prepareStatement(SentenciaSQL);
        sentencia.setString(1, raza.getRaza());
        sentencia.setString(2, raza.getSkill_1());
        sentencia.setString(3, raza.getSkill_2());
        sentencia.setString(4, raza.getSkill_3());
        sentencia.setString(5, String.valueOf(raza.getSkill_ex()));
        sentencia.setString(6, clave);
        sentencia.executeUpdate();
        System.out.println(raza.getRaza());
        modificarUsuario(clave, "raza", raza.getRaza());
        if (sentencia != null)
            sentencia.close();
    }

    /**
     * Elimina una clase y pone otra
     * @param clave id de Clase
     * @param clase clase Clase
     */
    public void modificar(String clave, Clase clase) throws SQLException {
        String SentenciaSQL = "UPDATE clase SET " +
                "clase = ?, " +
                "salud = ?, " +
                "armadura = ?, " +
                "magia = ?, " +
                "magias = ? " +
                "WHERE clase = ?";
        PreparedStatement sentencia = conexion.prepareStatement(SentenciaSQL);
        sentencia.setString(1, clase.getNombre());
        sentencia.setFloat(2, clase.getSalud());
        sentencia.setString(3, String.valueOf(clase.getArmadura()));
        sentencia.setBoolean(4, clase.isMagia());
        sentencia.setString(5, String.valueOf(clase.getTipoMagia()));
        sentencia.setString(6, clave);
        modificarUsuario(clave, "clase", clase.getNombre());
        sentencia.executeUpdate();
        if (sentencia != null)
            sentencia.close();
    }

    /**
     * Elimina un usuario y pone otro
     * @param clave id de Usuario
     * @param usuario clase Usuario
     */
    public void modificar(String clave, Usuario usuario) throws SQLException {
        String SentenciaSQL = "UPDATE usuario SET " +
                "nombre = ?, " +
                "apellido = ?, " +
                "fecha = ?, " +
                "raza = ?, " +
                "clase = ? " +
                "WHERE nombre = ?";
        PreparedStatement sentencia = conexion.prepareStatement(SentenciaSQL);
        sentencia.setString(1, usuario.getNombre());
        sentencia.setString(2, usuario.getApellidos());
        java.sql.Date fecha = new java.sql.Date(usuario.getFecha().getTime());
        sentencia.setDate(3, fecha);
        try{
            sentencia.setString(4, usuario.getRaza().getRaza());
        }catch (Exception e){
            sentencia.setString(4, "");
        }
        try{
            sentencia.setString(5, usuario.getClase().getNombre());
        }catch (Exception e){
            sentencia.setString(5, "");
        }
        sentencia.setString(6, clave);
        sentencia.executeUpdate();
        if (sentencia != null)
            sentencia.close();
    }

    private void modificarUsuario(String clave, String a, String b) throws SQLException {
        String sentenciaSQL;
        if (a.equalsIgnoreCase("clase"))
            sentenciaSQL = "UPDATE usuario SET clase = ? WHERE clase = ?";
        else
            sentenciaSQL = "UPDATE usuario SET raza = ? WHERE raza = ?";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        sentencia.setString(1, b);
        sentencia.setString(2, clave);
        sentencia.executeUpdate();
        if (sentencia != null)
            sentencia.close();
    }

    /**
     * Devuelve todos las clases
     * @return array de clases
     * @throws SQLException posible error SQL
     */
    public Collection<Clase> obtenerClase() throws SQLException {
        String sentenciaSQL = "SELECT * FROM clase";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        ResultSet rs = sentencia.executeQuery();
        ArrayList<Clase> coleccion = new ArrayList<>();
        while (rs.next()){
            Clase clase = new Clase();
            clase.setNombre(rs.getString(1));
            clase.setSalud(rs.getFloat(2));
            if (rs.getString(3).equalsIgnoreCase(Clase.Armadura.Pesada.toString())){
                clase.setArmadura(Clase.Armadura.Pesada);
            }else if (rs.getString(3).equalsIgnoreCase(Clase.Armadura.Media.toString())){
                clase.setArmadura(Clase.Armadura.Media);
            }else if ((rs.getString(3).equalsIgnoreCase(Clase.Armadura.Ligera.toString()))){
                clase.setArmadura(Clase.Armadura.Ligera);
            }
            clase.setMagia(rs.getBoolean(4));
            clase.setTipoMagia(Util.identificarMagia(rs.getString(5)));
            coleccion.add(clase);
        }
        return coleccion;
    }

    /**
     * Devuelve todas la razas
     * @return Array de razas
     * @throws SQLException posible error SQL
     */
    public Collection<Raza> obtenerRaza() throws SQLException {
        String sentenciaSQL = "SELECT * FROM raza";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        ResultSet rs = sentencia.executeQuery();
        ArrayList<Raza> coleccion = new ArrayList<>();
        while (rs.next()){
            Raza raza = new Raza();
            raza.setRaza(rs.getString(1));
            raza.setSkill_1(rs.getString(2));
            raza.setSkill_2(rs.getString(3));
            raza.setSkill_3(rs.getString(4));
            raza.setSkill_ex(Util.obtenerSkill(rs.getString(5)));
            coleccion.add(raza);
        }
        return coleccion;
    }

    /**
     * Devuelve todos los usuarios
     * @return array de los usuarios
     * @throws SQLException posible error SQL
     */
    public Collection<Usuario> obtenerUsuario() throws SQLException {
        String sentenciaSQL = "SELECT * FROM usuario";
        PreparedStatement sentencia = conexion.prepareStatement(sentenciaSQL);
        ResultSet rs = sentencia.executeQuery();
        ArrayList<Usuario>coleccion = new ArrayList<>();
        while (rs.next()){
            Usuario usuario = new Usuario();
            usuario.setNombre(rs.getString(1));
            usuario.setApellidos(rs.getString(2));
            java.util.Date fecha = new java.util.Date(rs.getDate(3).getTime());
            usuario.setFecha(fecha);
            if (!rs.getString(4).equals("")){
                Raza raza = obtenerRaza(rs.getString(4)).get(0);
                usuario.setRaza(raza);
            }
            if (!rs.getString(5).equals("")){
                Clase clase = obtenerClase(rs.getString(5)).get(0);
                usuario.setClase(clase);
            }
            coleccion.add(usuario);
        }
        return coleccion;
    }

    /**
     * Obtener usuario dada una cadena
     * @param cadena la susodicha cadena
     * @return un arrayList
     */
    public ArrayList<Usuario> obtenerUsuario(String cadena) throws SQLException {
        ArrayList<Usuario>user = new ArrayList<>();
        for (Usuario usuario : obtenerUsuario()){
            if (usuario.getNombre().contains(cadena) || usuario.getApellidos().contains(cadena)){
                user.add(usuario);
            }
        }
        return user;
    }

    /**
     * Obtener Clase dada una cadena
     * @param cadena la susodicha cadena
     * @return
     */
    public ArrayList<Clase> obtenerClase(String cadena) throws SQLException {
        ArrayList<Clase>clas = new ArrayList<>();
        for (Clase clase : obtenerClase()){
            if (clase.getNombre().contains(cadena)){
                clas.add(clase);
            }
        }
        return clas;
    }

    /**
     * Obtener Raza dada una cadena
     * @param cadena la susodicha cadena
     * @return
     */
    public ArrayList<Raza> obtenerRaza(String cadena) throws SQLException {
        ArrayList<Raza> raz = new ArrayList<>();
        for (Raza raza : obtenerRaza()){
            if (raza.getRaza().contains(cadena)){
                raz.add(raza);
            }
        }
        return raz;
    }

    /**
     * Carga desde la base de datos
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws SQLException
     */
    public void conectarBBDD() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException, IOException {
        if (Constantes.BASE_DATOS.equalsIgnoreCase("MySQL")){
            crearBaseDatosMySQL();
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/mmo",
                    "root",
                    "");
        }else{
            crearBaseDatosPostgreSQL();
            Class.forName("org.postgresql.Driver").newInstance();
            conexion = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/mmo",
                    "root",
                    "");
        }

    }

    private void crearBaseDatosPostgreSQL() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException, IOException{
        Class.forName("org.postgresql.Driver").newInstance();
        Connection conexion1 = DriverManager.getConnection(
                "jdbc:mysql://localhost:5432",
                "root",
                "");
        BufferedReader buffer = new BufferedReader(new FileReader(new File("BBDD.sql")));
        String frase="";
        String linea;
        while ((linea = buffer.readLine()) != null){
            frase = frase + linea;
        }
        String[] sentencia = frase.split(";");
        for (int i=0; i<sentencia.length; i++){
            String sentenciaSQL = sentencia[i];
            PreparedStatement ps = conexion1.prepareStatement(sentenciaSQL);
            ps.execute();
            ps.close();
        }
        conexion1.close();
    }

    private void crearBaseDatosMySQL() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException, IOException {
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        Connection conexion1 = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306",
                "root",
                "");
        BufferedReader buffer = new BufferedReader(new FileReader(new File("BBDD.sql")));
        String frase="";
        String linea;
        while ((linea = buffer.readLine()) != null){
            frase = frase + linea;
        }
        String[] sentencia = frase.split(";");
        for (int i=0; i<sentencia.length; i++){
            String sentenciaSQL = sentencia[i];
            PreparedStatement ps = conexion1.prepareStatement(sentenciaSQL);
            ps.execute();
            ps.close();
        }
        conexion1.close();
    }

    /**
     * Creacion del archivo PROPS para la lectura
     * @param ruta ruta
     * @throws IOException
     */
    public void crearProps(String ruta) throws IOException {
        this.config = new Properties();
        config.setProperty("user_login", Constantes.DEF_USER);
        config.setProperty("user_pwd", Constantes.DEF_PWD);
        config.setProperty("admin_login", Constantes.ADMIN_USER);
        config.setProperty("admin_pwd", Constantes.ADMIN_PWD);
        config.setProperty("BBDD", Constantes.BASE_DATOS);
        config.setProperty("ruta", ruta);
        config.store(new FileOutputStream(new File(RUTA_PROPS)),"Configuracion");
        RUTA_BASE=config.getProperty("ruta");
    }

    /**
     * Comprobacion de la existencia del archivo props
     * @param ruta ruta
     * @throws IOException
     */
    public void comprobacionFileConfig(String ruta) throws IOException {
        File file = new File(DEF_RUTA+File.separator+"Config.props");
        if(!file.exists()){
            crearProps(ruta);
        }
    }

    /**
     * Comprobacion del usuario y la cotraseña con el props
     * @param user Usuario
     * @param pwd Contraseña
     * @return
     * @throws IOException
     */
    public String comprobarUserPwd(String user,String pwd) throws IOException {
        this.config = new Properties();
        config.load(new FileInputStream(new File(RUTA_PROPS)));
        Constantes.BASE_DATOS=config.getProperty("BBDD");
        if (user.equals(config.getProperty("user_login")) && pwd.equals(config.getProperty("user_pwd"))) {
            return "usuario";
        } else if (user.equals(config.getProperty("admin_login")) && pwd.equals(config.getProperty("admin_pwd"))){
            return "admin";
        }
        return "error";
    }

}

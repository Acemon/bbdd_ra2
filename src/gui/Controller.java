package gui;

import base.*;
import util.Constantes;
import util.Util;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by DAM on 03/11/2016.
 */
public class Controller implements ActionListener, ChangeListener, ListSelectionListener, KeyListener{
    private Model model;
    private Ventana view;
    private Integer posicion;
    private boolean nuevo;
    private String clave;
    private boolean guardado;
    private boolean campoVacio;

    /**
     * Constructor del Controller de la aplicacion.
     * @param model Acceso a @Model predefinido de @App.
     * @param view Acceso a @Ventana predefinido de @App.
     */
    public Controller(Model model, Ventana view) {
        this.model=model;
        this.view=view;
        this.nuevo=false;
        addListeners();
        addActionCommands();
        try {
            model.comprobacionFileConfig(Constantes.DEF_RUTA);
        } catch (IOException e) {
            Util.MensajeError("Error al generar el PROPS");
        }
        activarInicial();
    }

    @Override
    /**
     * Realiza toda la operatoria con actionListeners como botones
     */
    public void actionPerformed(ActionEvent e) {
        Usuario usuario = null;
        Clase clase = null;
        Raza raza = null;
        String ruta = "";
        switch (e.getActionCommand()){
            case "Cambiar":
                if (view.rbPostgre.isSelected())
                    Constantes.BASE_DATOS="Postgre";
                else
                    Constantes.BASE_DATOS="MySQL";
                try {
                    model.crearProps(Constantes.DEF_RUTA);
                } catch (IOException e1) {
                    Util.MensajeError("Error al crear el archivo props");
                }
                break;
            case "Conectar":
                String contrasena = new String(view.tfContrasena.getPassword());
                try {
                    switch (model.comprobarUserPwd(view.tfUsuario.getText(), contrasena)){
                        case "admin":
                            view.lbbdd.setVisible(true);
                            view.rbMysql.setVisible(true);
                            view.rbPostgre.setVisible(true);
                            view.btCambiar.setVisible(true);
                        case "usuario":
                            try {
                                model.conectarBBDD();
                            } catch (ClassNotFoundException e1) {
                                Util.MensajeError("Error al encontrar la base de datos");
                            } catch (IllegalAccessException e1) {
                                Util.MensajeInfo("base de datos inaccesible con este usuario (acceso restringido)");
                            } catch (InstantiationException e1) {
                                Util.MensajeError("Error, avise a su programador java mas cercano");
                            } catch (SQLException e1) {
                                Util.MensajeInfo("El codigo SQL lanzado contiene errores");
                            }
                            refrescardlmdtm();
                            view.btConectar.setEnabled(false);
                            view.tfUsuario.setEnabled(false);
                            view.tfContrasena.setEnabled(false);
                            view.tabbedPane1.setEnabled(true);
                            activarCampos(false);
                            view.laviso.setText("Conectado");
                            view.archivo.setEnabled(true);
                            view.xml.setEnabled(true);
                            view.sql.setEnabled(true);
                            break;
                    }
                } catch (IOException e1) {
                    Util.MensajeError("Error al acceder al PROPS");
                }
                break;
            case "Anadir":
                view.btModificar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.tabbedPane1.setEnabled(false);
                activarCampos(true);
                limpiarTablas();
                refrescarcb();
                nuevo=true;
                view.laviso.setText("Modo Añadir");
                break;
            case "Modificar":
                view.btModificar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.tabbedPane1.setEnabled(false);
                if (posicion==0){
                    clave=view.tfUsuNombre.getText();
                }else if (posicion==1){
                    clave=view.tfClaNombre.getText();
                }else{
                    clave=view.tfRazNom.getText();
                }
                activarCampos(true);
                nuevo=false;
                view.laviso.setText("Modo Modificar");
                break;
            case "Guardar":
                //Usuario
                campoVacio=false;
                if (posicion==0){
                    usuario= new Usuario();
                    usuario=rellenar(usuario);
                    if (campoVacio){
                        Util.MensajeError("Faltan datos");
                        break;
                    }else {
                        if (nuevo)
                            try {
                                model.registrar(usuario);
                            } catch (SQLException e1) {
                                Util.MensajeInfo("El codigo SQL lanzado contiene errores");
                            }
                        if (!nuevo)
                            try {
                                model.modificar(clave, usuario);
                            } catch (SQLException e1) {
                                Util.MensajeInfo("El codigo SQL lanzado contiene errores");
                            }
                    }

                //Clase
                }else if (posicion==1){
                    clase = new Clase();
                    clase=rellenar(clase);
                    if (campoVacio){
                        break;
                    }else{
                        if (nuevo)
                            try {
                                model.registrar(clase);
                            } catch (SQLException e1) {
                                e1.printStackTrace();
                            }
                        if (!nuevo)
                            try {
                                model.modificar(clave, clase);
                            } catch (SQLException e1) {
                                e1.printStackTrace();
                            }
                    }
                //Raza
                }else{
                    raza = new Raza();
                    raza=rellenar(raza);
                    if (campoVacio){
                        Util.MensajeError("Faltan datos");
                        break;
                    }else{
                        if (nuevo)
                            try {
                                model.registrar(raza);
                            } catch (SQLException e1) {
                                e1.printStackTrace();
                            }
                        if (!nuevo)
                            try {
                                model.modificar(clave, raza);
                            } catch (SQLException e1) {
                                e1.printStackTrace();
                            }
                    }
                }
                view.tabbedPane1.setEnabled(true);
                limpiarTablas();
                limpiarcb();
                refrescardlmdtm();
                activarCampos(false);
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.laviso.setText("Guardado");
                break;
            case "Cancelar":
                view.tabbedPane1.setEnabled(true);
                limpiarTablas();
                limpiarcb();
                refrescardlmdtm();
                activarCampos(false);
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                view.laviso.setText("Guardado");
                break;
            case "Eliminar":
                if (posicion==0){
                    usuario = (Usuario) view.lBuscador.getSelectedValue();
                    try {
                        model.eliminarUsuario(usuario.getNombre());
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }else if (posicion==1){
                    clase = (Clase) view.lBuscador.getSelectedValue();
                    try {
                        model.eliminarClase(clase.getNombre());
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }else{
                    raza = (Raza) view.lBuscador.getSelectedValue();
                    try {
                        model.eliminarRaza(raza.getRaza());
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                }
                activarCampos(false);
                refrescardlmdtm();
                view.laviso.setText("Eliminado");
                view.btGuardar.setEnabled(false);
                view.btEliminar.setEnabled(false);
                break;
            case "Exportxml":
//                try {
  //                  model.exportarXML();
    //            } catch (ParserConfigurationException e1) {
      //              e1.printStackTrace();
        //        } catch (TransformerException e1) {
          //          e1.printStackTrace();
            //    }
                break;
            case "ExportSql":
                //WIP
                break;
        }
    }

    /**
     * hace que el usuario escoja una ruta
     * @return ruta
     */
    private String cambiarRuta() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(chooser);
        return chooser.getSelectedFile().getAbsolutePath();
    }

    @Override
    /**
     * sirve para saber en que pagina del tabbedpane estas
     */
    public void stateChanged(ChangeEvent changeEvent) {
        if (changeEvent.getSource()==view.chkMagia){
            if (view.chkMagia.isSelected()){
                view.cbClaMagia.setVisible(true);
                view.lmagia.setVisible(true);
            }else{
                view.cbClaMagia.setVisible(false);
                view.lmagia.setVisible(false);
            }
        }else{
            posicion=view.tabbedPane1.getSelectedIndex();
            if (posicion==0){
                view.lBuscador.setModel(view.dlmUsuario);
                view.tLista.setModel(view.dtmUsuario);
            }else if (posicion==1){
                view.lBuscador.setModel(view.dlmClase);
                view.tLista.setModel(view.dtmClase);
            }else{
                view.lBuscador.setModel(view.dlmRaza);
                view.tLista.setModel(view.dtmRaza);
            }
            limpiarcb();
            view.btModificar.setEnabled(false);
            view.btEliminar.setEnabled(false);
        }

    }
    @Override
    /**
     * Permite escoger un valor de la lista
     */
    public void valueChanged(ListSelectionEvent listSelectionEvent) {
        if(listSelectionEvent.getValueIsAdjusting()){
            return;
        }
        refrescarcb();
        limpiarTablas();
        if (posicion==0){
            Usuario usuario = (Usuario) view.lBuscador.getSelectedValue();
            if (usuario==null)
                return;
            view.tfUsuNombre.setText(usuario.getNombre());
            view.tfUsuApellido.setText(usuario.getApellidos());
            view.tfUsuFecha.setDate(usuario.getFecha());
            view.cbUsuRaza.setSelectedItem(usuario.getRaza());
            view.cbUsuClase.setSelectedItem(usuario.getClase());
        }else if (posicion==1){
            Clase clase = (Clase) view.lBuscador.getSelectedValue();
            if (clase==null)
                return;
            view.tfClaNombre.setText(clase.getNombre());
            view.tfClaVida.setText(String.valueOf(clase.getSalud()));
            if (clase.getArmadura().equals(Clase.Armadura.Ligera))
                view.rbLigera.setSelected(true);
            else if (clase.getArmadura().equals(Clase.Armadura.Media))
                view.rbMedia.setSelected(true);
            else
                view.rbPesada.setSelected(true);
            if (clase.isMagia()){
                view.chkMagia.setSelected(true);
                view.cbClaMagia.setSelectedItem(clase.getTipoMagia());
            }
        }else{
            Raza raza = (Raza) view.lBuscador.getSelectedValue();
            if (raza==null)
                return;
            view.tfRazNom.setText(raza.getRaza());
            view.tfRazSkill1.setText(raza.getSkill_1());
            view.tfRazSkill2.setText(raza.getSkill_2());
            view.tfRazSkill3.setText(raza.getSkill_3());
            view.cbRazSkill.setSelectedItem(raza.getSkill_ex());
        }
        if (!view.tfUsuNombre.getText().equals("") || !view.tfClaNombre.getText().equals("") ||
                !view.tfRazNom.getText().equals("")){
            view.btModificar.setEnabled(true);
            view.btEliminar.setEnabled(true);
        }
    }

    /**
     * rellena un usuario
     * @param usuario usuario vacio
     * @return usuario rellenado
     */
    private Usuario rellenar(Usuario usuario) {
        usuario.setNombre(view.tfUsuNombre.getText());
        usuario.setApellidos(view.tfUsuApellido.getText());
        usuario.setFecha(view.tfUsuFecha.getDate());
        usuario.setRaza((Raza) view.cbUsuRaza.getSelectedItem());
        usuario.setClase((Clase) view.cbUsuClase.getSelectedItem());
        if (usuario.getNombre().equals("") || usuario.getApellidos().equals("") || usuario.getFecha().equals("")){
            this.campoVacio=true;
        }
        return usuario;
    }

    /**
     * rellena una clase
     * @param clase Clase vacia
     * @return Clase llena
     */
    private Clase rellenar(Clase clase) {
        clase.setNombre(view.tfClaNombre.getText());
        if (clase.getNombre().equals("")){
            campoVacio=true;
        }
        if (view.tfClaVida.getText()==""){
            clase.setSalud((float) 0);
        }else {
            try{
                clase.setSalud(Float.parseFloat(view.tfClaVida.getText()));
            }catch (Exception e){
                this.campoVacio=true;
                Util.MensajeError("Has introducido una cadena de texto en un campo numerico");
            }
        }
        if (view.rbLigera.isSelected()) {
            clase.setArmadura(Clase.Armadura.Ligera);
        }else if (view.rbMedia.isSelected()) {
            clase.setArmadura(Clase.Armadura.Media);
        }else if (view.rbPesada.isSelected()){
            clase.setArmadura(Clase.Armadura.Pesada);
        }else{
            Util.MensajeError("No has seleccionado una armadura");
            this.campoVacio=true;
        }
        clase.setMagia(view.chkMagia.isSelected());
        if (clase.isMagia()){
            clase.setTipoMagia((Magias) view.cbClaMagia.getSelectedItem());
        }else{
            clase.setTipoMagia(null);
        }
        return clase;
    }

    /**
     * rellena una raza
     * @param raza raza vacia
     * @return raza llena
     */
    private Raza rellenar(Raza raza) {
        raza.setRaza(view.tfRazNom.getText());
        raza.setSkill_1(view.tfRazSkill1.getText());
        raza.setSkill_2(view.tfRazSkill2.getText());
        raza.setSkill_3(view.tfRazSkill3.getText());
        raza.setSkill_ex((Habilidades) view.cbRazSkill.getSelectedItem());
        if (raza.getRaza().equals("") || raza.getSkill_1().equals("") ||
                raza.getSkill_2().equals("") || raza.getSkill_3().equals("")){
            this.campoVacio=true;
        }
        return raza;
    }

    /**
     * Pone los action command con los que yo quiero trabajar
     */
    private void addActionCommands() {
        view.btConectar.setActionCommand("Conectar");
        view.btAnadir.setActionCommand("Anadir");
        view.btModificar.setActionCommand("Modificar");
        view.btGuardar.setActionCommand("Guardar");
        view.btEliminar.setActionCommand("Eliminar");
        view.btCancelar.setActionCommand("Cancelar");
        view.btGuardaManual.setActionCommand("GuardaManual");
        view.btCambiar.setActionCommand("Cambiar");

        view.exportxml.setActionCommand("Exportxml");
        view.exportsql.setActionCommand("ExportSql");
        view.cambiarRutaTrabajo.setActionCommand("GuardarComo");
    }

    /**
     * pone los listerner a lo que lo necesite
     */
    private void addListeners() {
        view.btConectar.addActionListener(this);
        view.btAnadir.addActionListener(this);
        view.btModificar.addActionListener(this);
        view.btGuardar.addActionListener(this);
        view.btEliminar.addActionListener(this);
        view.btCancelar.addActionListener(this);
        view.btGuardaManual.addActionListener(this);
        view.lBuscador.addListSelectionListener(this);
        view.tabbedPane1.addChangeListener(this);
        view.tfBuscar.addKeyListener(this);
        view.btCambiar.addActionListener(this);

        view.exportxml.addActionListener(this);
        view.exportsql.addActionListener(this);
        view.chkMagia.addChangeListener(this);
        view.cambiarRutaTrabajo.addActionListener(this);
    }

    /**
     * preparacion de lo que quiero que se vea al iniciar y lo que no
     */
    private void activarInicial() {
        this.clave="";
        this.posicion=0;
        this.guardado=true;
        this.campoVacio=false;
        view.tfUsuFecha.setEnabled(false);
        view.btAnadir.setEnabled(false);
        view.btModificar.setEnabled(false);
        view.lguardado.setVisible(false);
        view.btGuardaManual.setVisible(false);
        view.tabbedPane1.setEnabled(false);
        view.lBuscador.setModel(view.dlmUsuario);
        view.tLista.setModel(view.dtmUsuario);

        view.lbbdd.setVisible(false);
        view.rbMysql.setVisible(false);
        view.rbPostgre.setVisible(false);
        view.btCambiar.setVisible(false);

        view.archivo.setEnabled(false);
        view.xml.setEnabled(false);
        view.sql.setEnabled(false);
    }

    /**
     * Activacion de campos generica
     * @param b booleano de activacion
     */
    private void activarCampos(boolean b) {
        view.tfUsuNombre.setEnabled(b);
        view.tfUsuApellido.setEnabled(b);
        view.tfUsuFecha.setEnabled(b);
        view.cbUsuRaza.setEnabled(b);
        view.cbUsuClase.setEnabled(b);

        view.tfClaNombre.setEnabled(b);
        view.tfClaVida.setEnabled(b);
        view.rbPesada.setEnabled(b);
        view.rbMedia.setEnabled(b);
        view.rbLigera.setEnabled(b);
        view.chkMagia.setEnabled(b);
        view.cbClaMagia.setEnabled(b);

        view.tfRazNom.setEnabled(b);
        view.tfRazSkill1.setEnabled(b);
        view.tfRazSkill2.setEnabled(b);
        view.tfRazSkill3.setEnabled(b);
        view.cbRazSkill.setEnabled(b);

        view.btAnadir.setEnabled(!b);
        view.tfBuscar.setEnabled(!b);
        view.lBuscador.setEnabled(!b);
        view.btGuardar.setEnabled(b);
        view.btCancelar.setEnabled(b);
    }

    /**
     * Limpia las tablas
     */
    private void limpiarTablas() {
        view.tfUsuNombre.setText("");
        view.tfUsuApellido.setText("");
        view.tfUsuFecha.setDate(null);

        view.tfClaNombre.setText("");
        view.tfClaVida.setText("");
        view.armor.clearSelection();
        view.chkMagia.setSelected(false);

        view.tfRazNom.setText("");
        view.tfRazSkill1.setText("");
        view.tfRazSkill2.setText("");
        view.tfRazSkill3.setText("");
    }

    /**
     * limpia las combo box
     */
    private void limpiarcb() {
        view.cbUsuClase.removeAllItems();
        view.cbUsuRaza.removeAllItems();
        view.cbClaMagia.removeAllItems();
        view.cbRazSkill.removeAllItems();
    }

    /**
     * rellena las combo box
     */
    private void refrescarcb() {
        limpiarcb();
        try {
            for (Clase clase : model.obtenerClase()){
                view.cbUsuClase.addItem(clase);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            for (Raza raza : model.obtenerRaza()){
                view.cbUsuRaza.addItem(raza);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        for (Magias magia : Magias.values()){
            view.cbClaMagia.addItem(magia);
        }

        for (Habilidades hab : Habilidades.values()){
            view.cbRazSkill.addItem(hab);
        }

    }

    /**
     * refresca las DLM
     */
    private void refrescardlmdtm() {
        try {
            removedtmRows();

            view.dlmUsuario.removeAllElements();
                for (Usuario usuario : model.obtenerUsuario()){
                    view.dlmUsuario.addElement(usuario);
                    Object[]relleno ={
                            usuario.getNombre(),
                            usuario.getApellidos(),
                            usuario.getFecha(),
                            usuario.getRaza(),
                            usuario.getClase()
                    };
                    view.dtmUsuario.addRow(relleno);
                }

            view.dlmClase.removeAllElements();
                for (Clase clase : model.obtenerClase()){
                    view.dlmClase.addElement(clase);
                    Object[]relleno ={
                            clase.getNombre(),
                            clase.getSalud(),
                            clase.getArmadura(),
                            clase.isMagia(),
                            clase.getTipoMagia()
                    };
                    view.dtmClase.addRow(relleno);
                }

            view.dlmRaza.removeAllElements();
                for (Raza raza : model.obtenerRaza()){
                    view.dlmRaza.addElement(raza);
                    Object[]relleno ={
                            raza.getRaza(),
                            raza.getSkill_1(),
                            raza.getSkill_2(),
                            raza.getSkill_3(),
                            raza.getSkill_ex()
                    };
                    view.dtmRaza.addRow(relleno);
                }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void removedtmRows() {
        for (int i = view.dtmUsuario.getRowCount() - 1; i >= 0; i--){
            view.dtmUsuario.removeRow(i);
        }
        for (int i = view.dtmRaza.getRowCount() - 1; i >= 0; i--){
            view.dtmRaza.removeRow(i);
        }
        for (int i = view.dtmClase.getRowCount() - 1; i >= 0; i--){
            view.dtmClase.removeRow(i);
        }
    }

    @Override
    /**
     * sin uso
     */
    public void keyTyped(KeyEvent e) {

    }

    @Override
    /**
     * sin uso
     */
    public void keyPressed(KeyEvent e) {

    }

    @Override
    /**
     * funcionalidad de la tfbuscar
     */
    public void keyReleased(KeyEvent e) {
        String cadena= view.tfBuscar.getText();
        if (posicion==0){
            ArrayList<Usuario>usuario=null;
            if (cadena.length()<3){
                try {
                    usuario=new ArrayList<>(model.obtenerUsuario());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }else{
                try {
                    usuario=model.obtenerUsuario(cadena);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            view.dlmUsuario.clear();
            for (Usuario user : usuario){
                view.dlmUsuario.addElement(user);
            }
        }else if (posicion==1){
            ArrayList<Clase>clase=null;
            if (cadena.length()<3){
                try {
                    clase = new ArrayList<>(model.obtenerClase());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }else{
                try {
                    clase = model.obtenerClase(cadena);
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            view.dlmClase.clear();
            for (Clase clas : clase){
                view.dlmClase.addElement(clas);
            }
        }else{
            ArrayList<Raza> raza=null;
            if (cadena.length()<3){
                try {
                    raza = new ArrayList<>(model.obtenerRaza());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }else{
                try {
                    raza = (model.obtenerRaza(cadena));
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            view.dlmRaza.clear();
            for (Raza raz : raza){
                view.dlmRaza.addElement(raz);
            }
        }
    }
}
package gui;

import com.toedter.calendar.JDateChooser;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by Fernando on 08/11/2016.
 */
public class Ventana {
    JPanel panel1;
    JButton btConectar;
    JButton btAnadir;
    JButton btModificar;
    JButton btGuardar;
    JButton btEliminar;
    JTabbedPane tabbedPane1;
    JTextField tfUsuNombre;
    JTextField tfUsuApellido;
    JPasswordField tfContrasena;
    JTextField tfUsuario;
    JComboBox cbUsuClase;
    JTextField tfBuscar;
    JList lBuscador;
    JTextField tfClaNombre;
    JTextField tfRazNom;
    JTextField tfRazSkill1;
    JTextField tfRazSkill2;
    JTextField tfRazSkill3;
    JRadioButton rbPesada;
    JRadioButton rbLigera;
    JRadioButton rbMedia;
    JComboBox cbRazSkill;
    JComboBox cbUsuRaza;
    JCheckBox chkMagia;
    JComboBox cbClaMagia;
    JDateChooser tfUsuFecha;
    JTextField tfClaVida;
    JLabel laviso;
    JLabel lmagia;
    JButton btCancelar;
    JButton btGuardaManual;
    JLabel lguardado;
    JTable tLista;
    JRadioButton rbMysql;
    JRadioButton rbPostgre;
    JLabel lbbdd;
    JButton btCambiar;
    ButtonGroup armor;
    ButtonGroup BBDD;
    DefaultListModel dlmUsuario;
    DefaultListModel dlmClase;
    DefaultListModel dlmRaza;

    DefaultTableModel dtmUsuario;
    DefaultTableModel dtmClase;
    DefaultTableModel dtmRaza;

    JMenuBar menuBar;
    JMenu archivo;
    JMenuItem cambiarRutaTrabajo;
    JMenu xml;
    JMenuItem exportxml;
    JMenu sql;
    JMenuItem exportsql;

    public Ventana(){
        JFrame frame = new JFrame("Ventana");
        frame.setJMenuBar(getMenuBar());
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(3);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dlmUsuario = new DefaultListModel();
        dlmClase = new DefaultListModel();
        dlmRaza = new DefaultListModel();

        String[] columnaUsuario={"Nombre","Apellido","Fecha","Raza","Clase"};
        String[] columnaClase={"Clase","Salud","Armadura","Magia","Tipo"};
        String[] columnaRaza={"Raza","Hab. 1","Hab. 2","Hab. 3","Hab. especial"};
        dtmUsuario = new DefaultTableModel(null, columnaUsuario);
        dtmClase = new DefaultTableModel(null, columnaClase);
        dtmRaza = new DefaultTableModel(null, columnaRaza);

        armor = new ButtonGroup();
        armor.add(rbLigera);
        armor.add(rbMedia);
        armor.add(rbPesada);

        BBDD = new ButtonGroup();
        BBDD.add(rbMysql);
        BBDD.add(rbPostgre);


    }

    /**
     * Creacion del JMenuBar
     * @return JMenuBar
     */
    private JMenuBar getMenuBar() {
        menuBar = new JMenuBar();
        archivo = new JMenu("Archivo");
        cambiarRutaTrabajo = new JMenuItem("Cambiar ruta");
        xml = new JMenu("XML");
        exportxml = new JMenuItem("Exportar");
        sql = new JMenu("SQL");
        exportsql = new JMenuItem("Exportar");

        menuBar.add(archivo);
        archivo.add(cambiarRutaTrabajo);

        menuBar.add(xml);
        xml.add(exportxml);

        menuBar.add(sql);
        sql.add(exportsql);
        return menuBar;
    }
}

package base;

import java.io.Serializable;

/**
 * Created by Fernando on 08/11/2016.
 */
public class Clase implements Serializable{

    public enum Armadura {
        Pesada,
        Media,
        Ligera
    }
    private String nombre;
    private Float salud;
    private Armadura armadura;
    private boolean magia;
    private Magias tipoMagia;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getSalud() {
        return salud;
    }

    public void setSalud(Float salud) {
        this.salud = salud;
    }

    public Armadura getArmadura() {
        return armadura;
    }

    public void setArmadura(Armadura armadura) {
        this.armadura = armadura;
    }

    public boolean isMagia() {
        return magia;
    }

    public void setMagia(boolean magia) {
        this.magia = magia;
    }

    public Magias getTipoMagia() {
        return tipoMagia;
    }

    public void setTipoMagia(Magias tipoMagia) {
        this.tipoMagia = tipoMagia;
    }

    @Override
    public String toString() {
        return nombre;
    }
}

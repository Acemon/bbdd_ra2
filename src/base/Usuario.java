package base;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by DAM on 07/11/2016.
 */
public class Usuario implements Serializable{
    private String nombre;
    private String apellidos;
    private Date Fecha;
    private Raza raza;
    private Clase clase;

    public Usuario() {
        this.nombre = "";
        this.apellidos = "";
        Fecha = null;
        this.raza = null;
        this.clase = null;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getFecha() {
        return Fecha;
    }

    public void setFecha(Date fecha) {
        Fecha = fecha;
    }

    public Raza getRaza() {
        return raza;
    }

    public void setRaza(Raza raza) {
        this.raza = raza;
    }

    public Clase getClase() {
        return clase;
    }

    public void setClase(Clase clase) {
        this.clase = clase;
    }

    @Override
    public String toString() {
        return nombre+" "+apellidos;
    }
}

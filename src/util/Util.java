package util;

import base.Habilidades;
import base.Magias;

import javax.swing.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by DAM on 03/11/2016.
 */
public class Util {
    public static void MensajeError(String a){
        JOptionPane.showMessageDialog(null, a, "Error", JOptionPane.ERROR_MESSAGE);
    }
    public static void MensajeInfo(String a){
        JOptionPane.showMessageDialog(null, a, "Error", JOptionPane.INFORMATION_MESSAGE);
    }
    public static String formatFecha (Date fecha){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
        return sdf.format(fecha);
    }
    public static Date parseFecha (String fecha) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
        return sdf.parse(fecha);
    }

    public static Magias identificarMagia(String string) {
        if (string.equalsIgnoreCase(Magias.Blanca.toString())){
            return Magias.Blanca;
        }else if (string.equalsIgnoreCase(Magias.Negra.toString())){
            return Magias.Negra;
        }else if (string.equalsIgnoreCase(Magias.Roja.toString())){
            return Magias.Roja;
        }else if (string.equalsIgnoreCase(Magias.Azul.toString())){
            return Magias.Azul;
        }else if (string.equalsIgnoreCase(Magias.Temporal.toString())){
            return Magias.Temporal;
        }else if (string.equalsIgnoreCase(Magias.Arcana.toString())){
            return Magias.Arcana;
        }else if (string.equalsIgnoreCase(Magias.Sacro.toString())){
            return Magias.Sacro;
        }
        return null;
    }

    public static Habilidades obtenerSkill(String string) {
        if (string.equalsIgnoreCase(Habilidades.Escalada.toString())){
            return Habilidades.Escalada;
        } else if (string.equalsIgnoreCase(Habilidades.Planeo.toString())){
            return Habilidades.Planeo;
        } else if (string.equalsIgnoreCase(Habilidades.Rapidez.toString())){
            return Habilidades.Rapidez;
        } else if (string.equalsIgnoreCase(Habilidades.Respiracion.toString())){
            return Habilidades.Respiracion;
        }
        return null;
    }
}

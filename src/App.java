import gui.Controller;
import gui.Model;
import gui.Ventana;

/**
 * Created by Fernando on 08/11/2016.
 */
public class App {
    public static void main (String args[]){
        Model model = new Model();
        Ventana view = new Ventana();
        Controller controller = new Controller(model, view);
    }
}
